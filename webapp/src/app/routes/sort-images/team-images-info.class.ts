export class TeamImagesInfo {
    public id             : string                 = "";
    public name           : string                 = "";
    public imagesOk       : number                 = 0;
    public imagesTotal    : number                 = 0;
    public pointsRound    : number                 = 0;
}
