export class UserInfo {
    public id        : string = "";
    public name      : string = "";
    public team      : string = "";
    public connected : boolean = false;
};
